#!/bin/bash

# this script is only tested on an Amazon EC2 Instance with the Amazon Linux 2 or Amazon Linux AMI.

# to install Docker on an Amazon EC2 instance.

#update the installed packages and package cache on your instance.
sudo yum update -y

# install the most recent Docker Engine package.
# Amazon Linux 2
sudo amazon-linux-extras install docker

# to ensure that the Docker daemon starts after each system reboot.
sudo systemctl enable docker

# start the Docker service.
sudo service docker start

# to add user to docker group
usermod -aG docker ec2-user

# run jenkins
mkdir -p /var/jenkins_home
chown -R 1000:1000 /var/jenkins_home/
docker run -p 8080:8080 -p 50000:50000 -v /var/jenkins_home:/var/jenkins_home -d --name jenkins jenkins/jenkins:lts

# show endpoint
echo 'Jenkins installed'
echo 'You should now be able to access jenkins at: http://'$(curl -s ifconfig.co)':8080'

